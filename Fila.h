#include<stdio.h>

#define TAM 10

typedef struct fila{
	int fila[TAM];
	int inicio;
	int fim;
}Fila;

void inicia(Fila *f);
void insere(Fila *f, int elem);
void retira(Fila *f);
void imprimeinicio(Fila *f);

void inicia(Fila *f){
	f->inicio = 0;
	f->fim = -1;
}

void insere(Fila *f, int elem){
	if(f->fim == TAM-1){
		printf("A Fila está Cheia!/n");
	}else{
		f->fim++;
		f->fila[f->fim] = elem;
	}
}

void retira(Fila *f){
	f->inicio++;
}

void imprimeinicio(Fila *f){
	printf(" \n%d\n",f->fila[f->inicio]);
}